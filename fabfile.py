from __future__ import with_statement
from fabric.api import *

from fabric.contrib.console import confirm
from fabric.contrib.files import exists
from datetime import datetime
import sys, pprint, time, ConfigParser, os

def vagrant():

    # change from the default user to 'vagrant'
    env.user = 'vagrant'
    # connect to the port-forwarded ssh
    env.hosts = ['192.168.100.30']
 
    # use vagrant ssh key
    result = local('vagrant ssh-config efserver | grep IdentityFile', capture=True)
    env.key_filename = result.split()[1]


def sysinfo():
    run('uname -a')
    run('lsb_release -a')



def installBase():

    '''[create] Basic packages for building, version control'''
    with settings(warn_only=True):

        run("sudo apt-get -y update", pty = True)

        packages = [
            'build-essential',
            'subversion',
            'git',
            'unzip',
            'python-software-properties'
        ]

        packagelist = ' '.join(packages)

        run('sudo apt-get -y install %s' % packagelist, pty = True)

        sudo('add-apt-repository ppa:webupd8team/java')
        sudo('apt-get update')
        sudo('apt-get install oracle-java7-installer')

        run('java -version')



def configureApache():
    with settings(warn_only=True):

        sudo('a2enmod headers')
        sudo('a2enmod wsgi')
        sudo('a2dissite default')
        sudo('ln -s /vagrant/config/apache-site /etc/apache2/sites-available/dermsite')
        sudo('a2ensite dermsite')

def startapache():
    with settings(warn_only=True):
        sudo('service apache2 start')

